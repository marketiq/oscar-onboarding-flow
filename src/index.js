import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { Provider } from "react-redux";
import store from "./store";
import registerServiceWorker from "./registerServiceWorker";
import "react-toastify/dist/ReactToastify.css";
import "semantic-ui-css/semantic.min.css";

function noop() {}
if (process.env.NODE_ENV == "production") {
  console.log = noop;
  console.warn = noop;
  // console.error = noop;
}
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);

registerServiceWorker();
console.log("process.env.NODE_ENV: ", process.env.NODE_ENV);
