import firebase from "./config";
class Firebase {
  static trackWithProperties = async (event, params) => {
    firebase.analytics().logEvent(event, params);
  };

  reset = async event => {
    // this.mixpanel(() => Mixpanel.reset());
  };

  static customEvent = async (custom_event, dataObj) => {
    firebase.analytics().logEvent(custom_event, dataObj);
  };

  static set = async params => {
    firebase.analytics().setUserProperty(params.property, params.value);
  };

  static identify = async property => {
    firebase.analytics().setUserId(property.toString());
  };

  static setScreen = async (screenName, className) => {
    firebase.analytics().setCurrentScreen(screenName, className);
  };
  createAlias = async property => {
    // this.mixpanel(() => Mixpanel.createAlias(property));
  };
}

export default Firebase;
