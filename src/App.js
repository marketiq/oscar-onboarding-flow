import React, { Component } from "react";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import logo from "./logo.svg";
import "./App.css";
import AppBar from "material-ui/AppBar";
import AppRoutes from "./components/routes";
import WebFont from "webfontloader";

import { createBrowserHistory } from "history";
WebFont.load({
  google: {
    families: ["Roboto:100,300,400,500,500i,700", "sans-serif"]
  }
});
const history = createBrowserHistory();

class App extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <AppRoutes history={history} />
      </MuiThemeProvider>
    );
  }
}
export default App;
