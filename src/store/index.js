import { combineReducers, createStore, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import { createLogger } from "redux-logger";
import studentReducer from "./reducer/student";

let reducers = combineReducers({
  studentReducer
});

const loggerMiddleware = createLogger();
let store = createStore(
  reducers,
  applyMiddleware(thunkMiddleware, loggerMiddleware)
);

export default store;