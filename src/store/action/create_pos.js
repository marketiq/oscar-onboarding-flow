import { actionType } from "./types";
import { baseURL } from "../../constants";
import axios from "axios";
const PROXY_URL = "https://cors-anywhere.herokuapp.com/";
export default class POS {
  static createPOS = params => {
    return dispatch => {
      return new Promise(function(resolve, reject) {
        axios
          .post(
            `${baseURL}accounts/oscar/create/`,
            params
            // axiosConfig
          )
          .then(res => {
            if (res.status === 200) {
              resolve(res);
            } else {
              reject(res);
            }
          })
          .catch(err => {
            reject(err);
          });
      });
    };
  };
}
