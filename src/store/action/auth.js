import { actionType } from "./types";
import { baseURL } from "../../constants";
import axios from "axios";
const PROXY_URL = "https://cors-anywhere.herokuapp.com/";
export default class Auth {
  static onCreateUser = params => {
    return dispatch => {
      // axios.defaults.headers["Authorization"] = localStorage.getItem("token");
      let axiosConfig = {
        headers: {
          "Content-Type": "application/json;charset=UTF-8",
          "Access-Control-Allow-Origin": "*"
        }
      };
      return new Promise(function(resolve, reject) {
        axios
          .post(
            `${baseURL}accounts/oscar/create/`,
            params
            // axiosConfig
          )
          .then(res => {
            if (res.status === 200) {
              resolve(res);
            } else {
              reject(res);
            }
          })
          .catch(err => {
            reject(err);
          });
      });
    };
  };

  static check = params => {
    return dispatch => {
      // axios.defaults.headers["Authorization"] = localStorage.getItem("token");
      let axiosConfig = {
        headers: {
          "Content-Type": "application/json;charset=UTF-8",
          "Access-Control-Allow-Origin": "*"
        }
      };
      return new Promise(function(resolve, reject) {
        axios
          .post(
            `${baseURL}accounts/oscar/check/`,
            params
            // axiosConfig
          )
          .then(res => {
            resolve(res);
          })
          .catch(err => {
            reject(err);
          });
      });
    };
  };

  static sendotp = params => {
    return dispatch => {
      // axios.defaults.headers["Authorization"] = localStorage.getItem("token");
      let axiosConfig = {
        headers: {
          "Content-Type": "application/json;charset=UTF-8",
          "Access-Control-Allow-Origin": "*"
        }
      };
      return new Promise(function(resolve, reject) {
        axios
          .post(
            `${baseURL}accounts/oscar/sendotp/`,
            params
            // axiosConfig
          )
          .then(res => {
            resolve(res);
          })
          .catch(err => {
            reject(err);
          });
      });
    };
  };
  static verifyotp = params => {
    return dispatch => {
      // axios.defaults.headers["Authorization"] = localStorage.getItem("token");
      let axiosConfig = {
        headers: {
          "Content-Type": "application/json;charset=UTF-8",
          "Access-Control-Allow-Origin": "*"
        }
      };
      return new Promise(function(resolve, reject) {
        axios
          .post(
            `${baseURL}accounts/oscar/verifyotp/`,
            params
            // axiosConfig
          )
          .then(res => {
            resolve(res);
          })
          .catch(err => {
            reject(err);
          });
      });
    };
  };
}
