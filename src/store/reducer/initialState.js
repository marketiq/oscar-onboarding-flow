let initialState = {
  allStudents: [],
  gateAttendance: [],
  currentStudent: null,
  classAttendance: []
};

export default initialState;
