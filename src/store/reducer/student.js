import initialState from "./initialState";
import { actionType } from "../action/types";

function studentReducer(state = initialState.allStudents, action) {
  switch (action.type) {
    case actionType.GET_STUDENTS:
      return action.data;
    default:
      return state;
  }
}

export default studentReducer;
