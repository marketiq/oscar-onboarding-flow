import React from "react";
import { connect } from "react-redux";
import "./index.css";
import TextField from "material-ui/TextField";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";
import Header from "../header";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import FormControl from "@material-ui/core/FormControl";
import Auth from "../../store/action/auth";
import SkipBtn from "../skipBtn";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import Firebase from "../../Firebase/analyticsEvents";

class About extends React.Component {
  constructor(props) {
    super(props);
    var business_category =
      JSON.parse(localStorage.getItem("business_category")) || "";
    console.log("business_category: ", business_category);
    this.state = {
      selectedBusiness: business_category,
      disable: business_category ? false : true
    };
  }
  componentDidMount() {
    Firebase.setScreen("onboarding:BusinessCategoryScreen", "BusinessCategory");
  }

  selectBusiness = business_name => {
    this.setState({ selectedBusiness: business_name }, () => {
      Firebase.customEvent("chooseCategory", {
        category: business_name
      });
      let { selectedBusiness } = this.state;
      this.setState({ disable: !selectedBusiness ? true : false });
    });
  };

  next = () => {
    let { selectedBusiness } = this.state;
    localStorage.setItem(
      "business_category",
      JSON.stringify(this.state.selectedBusiness)
    );

    if (selectedBusiness === "Restaurant") {
      Firebase.customEvent("navigateToTabletScreen");
      this.props.changeView("TABLET");
      localStorage.setItem("viewType", JSON.stringify({ viewType: "TABLET" }));
    } else {
      Firebase.customEvent("navigateToCustomizeReceiptScreen");
      this.props.changeView("CUSTOMIZE_RECEIPT");
      localStorage.setItem(
        "viewType",
        JSON.stringify({ viewType: "CUSTOMIZE_RECEIPT" })
      );
    }
  };
  render() {
    let { errors } = this.state;
    console.log("this.state: ", this.state);
    return (
      <div>
        <Header history={this.props.history} title="Your Business" />
        <ReactCSSTransitionGroup
          transitionName="example"
          transitionAppear={true}
          transitionAppearTimeout={1000}
          transitionEnter={false}
          transitionLeave={false}
          // transitionEnterTimeout={500}
          // transitionLeaveTimeout={300}
        >
          <section className="maincont aboutMaincont">
            <div className="container">
              <div className="col-lg-12 col-sm-12">
                <div className="oscar-logo-wrapper">
                  <img src={require("../../assets/shop.svg")} />
                </div>
              </div>

              <div className="">
                <div className="topSectionBox">
                  <div className="back-btn-wrapper">
                    <div
                      className="backarrow"
                      onClick={() => {
                        Firebase.customEvent("backToAboutYourBusinessScreen");
                        this.props.changeView("ABOUT_YOURSELF")
                      }}
                    >
                      <img
                        className="backarrow-img"
                        src={require("../../assets/backarrow.svg")}
                      />
                    </div>
                  </div>

                  <div className="skipBoxMain">
                    {/* <SkipBtn history={this.props.history} /> */}
                  </div>
                  <div className="verification_topHeading">
                    <h1 className="formHeading">What do you do?</h1>
                    <h1 className="blankSpace"></h1>
                  </div>
                </div>
              </div>

              <div>
                <div className="businessHeading">
                  SELECT YOUR BUSINESS CATEGORY
                </div>
                <div className="business-category-container">
                  <div
                    onClick={() => this.selectBusiness("Restaurant")}
                    className="business-category-item"
                    style={{
                      boxShadow:
                        this.state.selectedBusiness === "Restaurant"
                          ? "0 1px 1px rgba(231, 191, 226, 0.9)"
                          : "",
                      border:
                        this.state.selectedBusiness === "Restaurant"
                          ? "1px solid #e7bfe2"
                          : "",
                      background:
                        this.state.selectedBusiness === "Restaurant"
                          ? "#f8f8f8"
                          : ""
                    }}
                  >
                    <div className="business-category-name-img">
                      <img
                        src={require("../../assets/cafe_and_restaurant.svg")}
                      />
                    </div>
                    <div className="business-category-name">
                      Cafe & Restaurant
                    </div>
                  </div>
                  <div
                    onClick={() => this.selectBusiness("grocery")}
                    className="business-category-item"
                    style={{
                      boxShadow:
                        this.state.selectedBusiness === "grocery"
                          ? "0 1px 1px rgba(231, 191, 226, 0.9)"
                          : "",
                      border:
                        this.state.selectedBusiness === "grocery"
                          ? "1px solid #e7bfe2"
                          : "",
                      background:
                        this.state.selectedBusiness === "grocery"
                          ? "#f8f8f8"
                          : ""
                    }}
                  >
                    <div className="business-category-name-img">
                      <img src={require("../../assets/grocery.svg")} />
                    </div>
                    <div className="business-category-name">
                      Grocery & Retail Store
                    </div>
                  </div>
                  <div
                    onClick={() => this.selectBusiness("Fashion")}
                    className="business-category-item"
                    style={{
                      boxShadow:
                        this.state.selectedBusiness === "Fashion"
                          ? "0 1px 1px rgba(231, 191, 226, 0.9)"
                          : "",
                      border:
                        this.state.selectedBusiness === "Fashion"
                          ? "1px solid #e7bfe2"
                          : "",
                      background:
                        this.state.selectedBusiness === "Fashion"
                          ? "#f8f8f8"
                          : ""
                    }}
                  >
                    <div className="business-category-name-img">
                      <img src={require("../../assets/fashion.svg")} />
                    </div>
                    <div className="business-category-name">
                      Fashion & Apparel
                    </div>
                  </div>
                  <div
                    onClick={() => this.selectBusiness("pharmacy")}
                    className="business-category-item"
                    style={{
                      boxShadow:
                        this.state.selectedBusiness === "pharmacy"
                          ? "0 1px 1px rgba(231, 191, 226, 0.9)"
                          : "",
                      border:
                        this.state.selectedBusiness === "pharmacy"
                          ? "1px solid #e7bfe2"
                          : "",
                      background:
                        this.state.selectedBusiness === "pharmacy"
                          ? "#f8f8f8"
                          : ""
                    }}
                  >
                    <div className="business-category-name-img">
                      <img src={require("../../assets/pharmacy.svg")} />
                    </div>
                    <div className="business-category-name">
                      Pharmacy & Health
                    </div>
                  </div>
                  <div
                    onClick={() => this.selectBusiness("salons")}
                    className="business-category-item"
                    style={{
                      boxShadow:
                        this.state.selectedBusiness === "salons"
                          ? "0 1px 1px rgba(231, 191, 226, 0.9)"
                          : "",
                      border:
                        this.state.selectedBusiness === "salons"
                          ? "1px solid #e7bfe2"
                          : "",

                      background:
                        this.state.selectedBusiness === "salons"
                          ? "#f8f8f8"
                          : ""
                    }}
                  >
                    <div className="business-category-name-img">
                      <img src={require("../../assets/salons.svg")} />
                    </div>
                    <div className="business-category-name">Salons & Spas</div>
                  </div>
                  <div
                    onClick={() => this.selectBusiness("other")}
                    className="business-category-item"
                    style={{
                      border:
                        this.state.selectedBusiness === "other"
                          ? "1px solid #e7bfe2"
                          : "",
                      boxShadow:
                        this.state.selectedBusiness === "other"
                          ? "0 1px 1px rgba(231, 191, 226, 0.9)"
                          : "",
                      background:
                        this.state.selectedBusiness === "other" ? "#f8f8f8" : ""
                    }}
                  >
                    <div className="business-category-name-img">
                      <img src={require("../../assets/other.svg")} />
                    </div>
                    <div className="business-category-name">Other</div>
                  </div>
                </div>
              </div>
              <div className="text_box_fields">
                <button
                  onClick={this.next}
                  disabled={this.state.disable}
                  className="nextBtn"
                  type="submit"
                  style={{
                    background: this.state.disable ? "#d1d3d4" : "#4ac600"
                  }}
                >
                  <div className="nextBtnText">NEXT</div>
                  <div className="nextBtnimg">
                    <img
                      className="nextBtn-img"
                      src={require("../../assets/nextarrow.svg")}
                    />
                  </div>
                </button>
              </div>
            </div>
          </section>
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}

export default connect()(About);
