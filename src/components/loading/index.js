import React from 'react'
import './loading.css';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';


class About extends React.Component {
  constructor(props){
    super(props);
    this.state= {
      value: '',
      valueone: ''
    }
  }
  handleChange = (event, index, value) => this.setState({value});
  handleChange2 = (event, index, valueone) => this.setState({valueone});

  render() {

    return (
        <section className="maincont">
          <div className="loading">
            <div className="loading_inner">
              <img className="loader" src="loader.gif" />
              <h4>
                LOADING...
              </h4>
              <p>
                Please wait! We’re setting things up for you.
              </p>
            </div>
          </div>
          <div className="container">
              <div className="row">
                  
                  <div className="col-lg-12">
                      <h1 className="crtposheading">
                          Share some business details so we can help you grow
                      </h1>
                  </div>

                  <div className="col-lg-6 offset-lg-3">
                      <form className="aboutaccountform">

                          <div className="drop_down_select_pos">
                            <SelectField
                              floatingLabelText="Where are you selling today ?"
                              value={this.state.value}
                              onChange={this.handleChange}
                              floatingLabelStyle={{color: '#A7A9AC'}}
                              underlineFocusStyle={{borderColor: '#9789dd'}}
                              underlineStyle={{borderColor: '#E7E7EF'}}
                              selectedMenuItemStyle={{color: '#9789dd'}}
                              labelStyle={{color: '#262626', paddingLeft: '12px', paddingBottom: '10px',}}
                              >
                              <MenuItem value={1} primaryText="In-Store" />
                              <MenuItem value={2} primaryText="Online" />
                            </SelectField>
                          </div>

                          <div className="drop_down_select_pos">
                            <SelectField
                            value={this.state.valueone}
                            onChange={this.handleChange2}
                              floatingLabelText="Which System do you use today for selling In-Store"
                              floatingLabelStyle={{color: '#A7A9AC'}}
                              underlineFocusStyle={{borderColor: '#9789dd'}}
                              underlineStyle={{borderColor: '#E7E7EF'}}
                              selectedMenuItemStyle={{color: '#9789dd'}}
                              labelStyle={{color: '#262626', paddingLeft: '12px', paddingBottom: '10px',}}
                              >
                              <MenuItem value={1} primaryText="Wordpress" />
                              <MenuItem value={2} primaryText="Magneto" />
                              <MenuItem value={3} primaryText="Etsy" />
                              <MenuItem value={4} primaryText="Wix" />
                              <MenuItem value={5} primaryText="BigCommerce" />
                              <MenuItem value={6} primaryText="WooCommerce" />
                              <MenuItem value={7} primaryText="Shopify" />
                              <MenuItem value={8} primaryText="Amazon" />
                            </SelectField>
                          </div>


                      </form>

                      <button className="formBtn" type="submit">CREATE MY POS</button>
                  </div>

              </div>


          </div>
        </section>
    );
  }
}   


export default About;
