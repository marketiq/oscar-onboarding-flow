import React from "react";
import "./styles.css";
import {getClient} from '../../constants'
class Home extends React.Component {
    routeToCreate=()=>{
      let client = getClient()
      if(client === 'resthero') {
        localStorage.setItem('client', 'restHero')
      }else {
        localStorage.setItem('client', '')
      }
        this.props.history.push('/create_account')
      }
  render() {
    return (
      <div className="main-div">
      <section className="maincont">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-sm-12">
              <div className="inner-container">
                <div className="align-center" >
                {/* <p className="welcome-text">Welcome!</p> */}
                  <p className="welcome-text">Thank you for your interest!</p>
                  {/* <p className="paragraph-text">Let's create your account for Exceed!</p> */}
                  <p className="paragraph-text">Someone from our team will contact you within the next 24 hour</p>
                  <p className="paragraph-text">if you wish to auto-onboard, please click the button below</p>
                  <button className="formBtn" onClick={this.routeToCreate}>Start Onboarding</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      </div>
    );
  }
}

export default Home;
