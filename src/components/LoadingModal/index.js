import React from "react";
import { connect } from "react-redux";

import { Button, Header, Image, Modal } from "semantic-ui-react";
import "./style.css";
// <div className="ui small text loader">Loading</div>

class LoadingModal extends React.Component {
  loadingBox = () => {
    let { type, loadingText } = this.props;

    switch (type) {
      case "loading":
        return (
          <div className="loadingBox_Main">
            <div>
              <img width="80px" src={require("../../assets/loader.png")} />
            </div>
            <h2>Loading...</h2>
            <div>{loadingText}</div>
          </div>
        );

      case "finished":
        return (
          <div className="loadingBox_last"
            style={{
              
            }}
          >
            
            
        <h1>Success! Your POS is being created.</h1>
        <h2>Please give us a few minutes while we create your POS.</h2>
        <hr style={{width: '300px', backgroundColor:'rgb(134, 128, 128)' , margin: '20px 0'}}/>
        <p style={{marginBottom: '10px'}}>You can close this page.</p>
        <p>We will email you within a few minutes.</p>
    
          </div>
        );

        default : 
        return (
          <div className="loadingBox_Main">
            <div>
              <img width="80px" src={require("../../assets/loader.png")} />
            </div>
            <h2>Loading...</h2>
            <div>{loadingText}</div>
          </div>
        );
    }
  };
  render() {
    console.log("LOADING MODAL : loading state ", this.props.isLoading);
    let { toggle, isLoading, type } = this.props;
    return (
      <Modal
        // open={this.props.loading}
        open={isLoading}
        basic
        dimmer={"blurring"}
        size={"large"}
        animation={"fade"}
        style={styles.modalContainer}
      >
        <Modal.Content>
          {/* <div className="ui center aligned grid"> */}
          {/* <Image
            className="loading-image"
            wrapped
            src={require("../../assets/loader.png")}
          /> */}
          {type === "finished" ? (
            <div
              style={{ textAlign: "right", cursor: "pointer" }}
              onClick={toggle}
            >
              <img width="20px" src={require("../../assets/close.svg")} />
            </div>
          ) : (
            ""
          )}
          <div
            style={{
              height: "100vh",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "column",
              overflowX: "hidden"
            }}
          >
            {this.loadingBox()}
          </div>
          {/* </div> */}
        </Modal.Content>
      </Modal>
    );
  }
}

const styles = {
  modalContainer: {
    // position: "absolute",
    // top: "50%",
    // bottom: 0,
    // left: 0,
    // right: 0,
    // margin: "-80px auto",
    // width: 160,
    // height: 160
  }
};

export default LoadingModal;
