import React, { Component } from "react";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
// import logo from "./logo.svg";
import "../../App.css";
import AppBar from "material-ui/AppBar";
import Header from "../header";
import CreateAccount from "../createaccount";
import About from "../about";
import CreatePOS from "../createpos";
import Loading from "../loading";
import BusinessCategory from "../business_category";
import CustomizeReceipt from "../customize_receipt";
import Verification from "../VerificationScreen";
import TabletScreen from "../TabletScreen";
import DineScreen from "../DineScreen";
import DeliveryScreen from "../DeliveryScreen";
import axios from "axios";
import ThankYou from "../thankYou";
import Login from "../login";
import { baseURL } from "../../constants";
import { Animated } from "react-animated-css";

class App extends Component {
  constructor(props) {
    super(props);
    if (document.referrer.match("rest-hero.oscarhub.org|oscar.pk") && !sessionStorage.onboarding_initiated) {
      this.state = { activeView: "CREATE_ACCOUNT", profile: {} };
      sessionStorage.onboarding_initiated = true;
    }
    else {
      let viewType = JSON.parse(localStorage.getItem("viewType"));
      this.state = {
        activeView: (viewType && viewType.viewType) || "CREATE_ACCOUNT",
        profile: {}
      };
    }
    this.renderView = this.renderView.bind(this);
    this.changeView = this.changeView.bind(this);
  }

  changeView(activeView, profile) {
    console.log('asc', activeView, profile);
    this.setState({
      activeView,
      profile: profile
        ? {
            ...this.state.profile,
            ...profile
          }
        : null
    });
  }

  renderView() {
    switch (this.state.activeView) {
      case "DELIVERY_TYPE":
        return (
          <DeliveryScreen
            history={this.props.history}
            changeView={this.changeView}
          />
        );
      case "DINE":
        return (
          <DineScreen
            history={this.props.history}
            changeView={this.changeView}
          />
        );
      case "TABLET":
        return (
          <TabletScreen
            history={this.props.history}
            changeView={this.changeView}
          />
        );
      case "VERIFICATION":
        return (
          <Verification
            history={this.props.history}
            changeView={this.changeView}
          />
        );
      case "CREATE_ACCOUNT":
        return (
          <CreateAccount
            history={this.props.history}
            changeView={this.changeView}
          />
        );
      case "ABOUT_YOURSELF":
        return (
          <About history={this.props.history} changeView={this.changeView} />
        );

      case "BUSINESS_CATEGORY":
        return (
          <BusinessCategory
            history={this.props.history}
            changeView={this.changeView}
          />
        );

      case "CUSTOMIZE_RECEIPT":
        return (
          <CustomizeReceipt
            history={this.props.history}
            changeView={this.changeView}
          />
        );
      case "CREATE_POS":
        return (
          <CreatePOS
            history={this.props.history}
            changeView={this.changeView}
          />
        );
      case "THANK_YOU":
        return <ThankYou changeView={this.changeView} />;
      default:
        return <Loading changeView={this.changeView} />;
    }
  }

  render() {
    return (
      <MuiThemeProvider>
        <div className="App">{this.renderView()}</div>
      </MuiThemeProvider>
    );
  }
}
const styles = {};
export default App;
