import React from "react";
import { Route, Switch, Router, Redirect } from "react-router-dom";
import Login from "../login";
import Main from "../main";
import Home from "../Home";

const isAuthenticated = () => {
  let token = localStorage.getItem("token");
  if (token) {
    return true;
  }
  return false;
};

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      true ? <Component {...props} /> : <Redirect to="/login" />
    }
  />
);

class AppRoutes extends React.Component {
  render() {
    return (
      <Router history={this.props.history}>
        <Switch>
          <Route path='/welcome' component={Home}/>
          <Route path="/login" component={Login} />
          <PrivateRoute path="/create_account" component={Main} />
          <PrivateRoute path="/" component={Main} />
        </Switch>
      </Router>
    );
  }
}

export default AppRoutes;
