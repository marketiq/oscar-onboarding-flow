import React from "react";
import Header from "../header";
import { patt, symbols, letterLower, letterUpper } from "../../constants";
import Auth from "../../store/action/auth";
import { connect } from "react-redux";
import { Animated } from "react-animated-css";
import ReactCodeInput from "react-code-input";
// import SkipBtn from "../skipBtn";
import LoadingModal from "../LoadingModal";
import { ToastContainer, toast } from "react-toastify";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import Firebase from "../../Firebase/analyticsEvents";
import "./style.css";

class Verification extends React.Component {
  constructor(props) {
    super(props);
    let user_info = JSON.parse(localStorage.getItem("user_info"));
    this.state = {
      resCode: "",
      phone_number: user_info && user_info.phone_number,
      invalidCode: false,
      isLoading: false,
      animationClasses: "example-appear"
    };
  }
  componentDidMount() {
    Firebase.setScreen("onboarding:CodeVerification", "Verification");
    this.sendCode();
    this.setState({
      animationClasses: `${this.state.animationClasses} example-appear-active`
    });
  }

  sendCode = (flag = null) => {
    let { phone_number } = this.state;
    this.props
      .dispatch(Auth.sendotp({ phone_number }))
      .then(res => {
        if (res.data.sent) {
          toast.success("Code has been sent", {
            position: "top-right",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
        console.log("sendotp response: ", res);
        if (flag) {
          Firebase.customEvent("resentVerificationCode", { phone_number });
        } else {
          Firebase.customEvent("sentVerificationCode", { phone_number });
        }
      })
      .catch(error => {
        console.log("sendotp error: ", error);
      });
  };
  onSubmit = e => {
    e.preventDefault();
    let { email, name, phone_number, city, store_name, password } = this.state;
    let params = {
      email,
      name,
      username: email,
      phone_number,
      password,
      city
    };

    if (!email || !name || !phone_number || !password) {
      console.log("inside if");
      this.setState({
        errors: {
          email: !email ? "This field is required." : "",
          name: !name ? "This field is required." : "",
          phone_number: !phone_number ? "This field is required." : "",
          store_name: !store_name ? "This field is required." : "",
          city: !city ? "This field is required." : "",
          password: !password ? "This field is required." : ""
        }
      });
      return;
    }
    if (!phone_number.match(patt)) {
      this.setState({
        errors: {
          phone_number: "Invalid Ph #. Retry with correct Ph # E.g 03001234567"
        },
        loading: false
      });
      return;
    }
    localStorage.setItem("user_info", JSON.stringify(params));
    let param = {
      email,
      username: name,
      phone_number,
      password
      // check: true
    };
    this.props
      .dispatch(Auth.check({ email }))
      .then(res => {
        console.log("check user response: ", res);
        if (!res.data.available) {
          this.setState({
            errors: {
              email: "email already exist please try with another email"
            }
          });
          return;
        }
        localStorage.setItem(
          "viewType",
          JSON.stringify({ viewType: "ABOUT_YOURSELF" })
        );
        this.props.changeView("ABOUT_YOURSELF");
      })
      .catch(err => {
        console.log("error: ", err);
      });
  };

  onChange = code => {
    console.log("onchange event: ", typeof code);
    let { invalidCode, phone_number } = this.state;
    if (invalidCode) {
      this.setState({ invalidCode: false });
    }
    if (code.toString().length === 4 && !isNaN(code)) {
      this.setState({ isLoading: true });
      this.props
        .dispatch(
          Auth.verifyotp({ otp: code, phone_number: this.state.phone_number })
        )
        .then(res => {
          console.log("verifyotp response: ", res);
          if (res.data.verified) {
            localStorage.setItem(
              "viewType",
              JSON.stringify({ viewType: "ABOUT_YOURSELF" })
            );
            this.setState({ isLoading: false });
            Firebase.identify(phone_number);
            Firebase.customEvent("validCodeEntered", { code: code.toString() });
            Firebase.customEvent("navigateToAboutYourBusinessScreen");
            this.props.changeView("ABOUT_YOURSELF");
          } else {
            this.setState({ invalidCode: true, isLoading: false });
            Firebase.customEvent("invalidCodeEntered", {
              code: code.toString()
            });
          }
        })
        .catch(error => {
          this.setState({ isLoading: false });
          console.log("verifyotp error: ", error);
        });
    }
  };

  callUs = () => {
    // window.open("tel:03463149015");
    window.location.href = "tel:03112802210";
  };
  render() {
    let { errors } = this.state;
    console.log("disable: ", this.state.disable);
    return (
      <div>
        <LoadingModal
          type="loading"
          // isLoading={true||this.state.isLoading}
          isLoading={this.state.isLoading}
          loadingText="Verifying your Code"
        />
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnVisibilityChange
          draggable
          pauseOnHover
        />
        <Header history={this.props.history} title="New Account" />
        <div className={this.state.animationClasses}>
          <section className="maincont verification_boxMain">
            <div className="container">
              <div>
                <div className="col-lg-12 col-sm-12">
                    <div className="oscar-logo-wrapper">
                      <img src={require("../../assets/oscar-logo.svg")} />
                    </div>
                </div>
                <div className="topSectionBox">
                  <div className="back-btn-wrapper">
                    <div
                      className="backarrow"
                      onClick={() => this.props.changeView("CREATE_ACCOUNT")}
                    >
                      <img
                        className="backarrow-img"
                        src={require("../../assets/backarrow.svg")}
                      />
                    </div>
                  </div>
                  <div className="verification_topHeading">
                    <h1 className="formHeading">
                      Enter Your Verification Code
                    </h1>
                    <h1 className="formHeading2">
                      we have sent a verification code to{" "}
                      <span className="verficationNumber">
                        {this.state.phone_number}
                      </span>
                    </h1>
                  </div>

                  {/* <div className="skipBoxMain">
                     <SkipBtn history={this.props.history} /> 
                  </div> */}

                  {/* <h1 className="formHeading">Lets customize your receipt</h1>
                  <h1 className="blankSpace"></h1> */}
                </div>

                <div className="">
                  {/* <h1 className="formHeading">Enter Your Verification Code</h1> */}

                  <div className="verification-code-wrapper">
                    <div className="verification-code-wrapper-row-1">
                      Verification Code
                    </div>
                    <div className="verification-code-wrapper-row-2">
                      <ReactCodeInput
                        filterChars={[
                          ...symbols,
                          ...letterLower,
                          ...letterUpper
                        ]}
                        fields={4}
                        type="text"
                        onChange={this.onChange}
                      />
                    </div>
                  </div>
                </div>
                <div
                  className="verfication_inpout_Error"
                  style={{
                    visibility: this.state.invalidCode ? "visible" : "hidden",
                    color: "red"
                  }}
                >
                  Invalid Code
                </div>
                <div className="resend-code-container">
                  <div className="resend-code-container-row-1">
                    <p>
                      I didn't get a code{" "}
                      <span onClick={() => this.sendCode("resend")}>
                        Resend Code
                      </span>
                    </p>
                  </div>
                  <div className="resend-code-container-row-2">
                    <p>
                      Having trouble? Call Us at
                      <span onClick={this.callUs}>0311-2802210</span>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    );
  }
}

export default connect()(Verification);
