import React from "react";
import { connect } from "react-redux";
import "./styleabout.css";
import TextField from "material-ui/TextField";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";
import Header from "../header";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import FormControl from "@material-ui/core/FormControl";
import Auth from "../../store/action/auth";
import { cities } from "../../constants";
import SkipBtn from "../skipBtn";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import Firebase from "../../Firebase/analyticsEvents";

class About extends React.Component {
  constructor(props) {
    super(props);
    var user_about = JSON.parse(localStorage.getItem("user_about"));
    console.log("abou_user: ", user_about);
    this.state = {
      business_name: (user_about && user_about.business_name) || "",
      business_type: (user_about && user_about.business_type) || "",
      errors: {},
      current_pos: (user_about && user_about.current_pos) || "",
      selling_type: (user_about && user_about.selling_type) || "",
      city: (user_about && user_about.city) || "Karachi",
      customize_url: (user_about && user_about.customize_url) || "",
      disable: user_about ? false : true
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
  }
  componentDidMount() {
    this.isDisable();
    Firebase.setScreen("onboarding:AboutYourBusinessScreen", "AboutBusiness");
  }

  onSubmit(e) {
    e.preventDefault();
    let {
      business_type,
      selling_type,
      current_pos,
      business_name,
      customize_url,
      city
    } = this.state;
    let data = {
      business_name,
      customize_url: customize_url.toLowerCase(),
      city
    };
    if (!business_name || !customize_url || !city) {
      this.setState({
        errors: {
          business_name: !business_name ? "This field is required." : "",
          customize_url: !customize_url ? "This field is required." : "",
          city: !city ? "This field is required." : ""
        }
      });
      return;
    }
    localStorage.setItem("user_about", JSON.stringify(data));
    this.props.dispatch(Auth.check({ business_name })).then(({ data }) => {
      if (!data.available) {
        this.setState({
          errors: { business_name: "business name should be unique" }
        });
        return;
      }
      this.props
        .dispatch(Auth.check({ customize_url }))
        .then(res => {
          console.log("check user response: ", res);
          if (!res.data.available) {
            this.setState({ errors: { customize_url: "url already exist" } });
            return;
          }
          // localStorage.setItem(
          //   "viewType",
          //   JSON.stringify({ viewType: "BUSINESS_CATEGORY" })
          // );
          localStorage.setItem(
            "viewType",
            JSON.stringify({ viewType: "CUSTOMIZE_RECEIPT" })
          );
          // Firebase.customEvent("navigateToBusinessCategoryScreen", data);

          Firebase.customEvent("navigateToCustomizeReceiptScreen");
          this.props.changeView("CUSTOMIZE_RECEIPT", data);
          // this.props.changeView("BUSINESS_CATEGORY", data);
          // this.props.changeView("DELIVERY_TYPE", data);//CUSTOMIZE_RECEIPT
        })
        .catch(err => {
          console.log("err from about: ", err);
        });
    });
  }
  onChange(e) {
    // "^[a-zA-Z0-9]*$"
    // let urlregex = RegExp("^[a-z0-9_.-]*$");
    if (this.state.customize_url.length > 50) {
      return;
    }
    // text.replace(/[^\w]/g, '')

    if (e.target.id === "customize_url") {
      this.setState({ ['customize_url']: e.target.value.toLowerCase().replace(/[^\w]/g, '') }, () => {
        this.isDisable();
      });
    } else {
      this.setState({ ['customize_url']: e.target.value.toLowerCase().replace(/[^\w]/g, '') }, () => {
        this.isDisable();
      });

      this.setState(
        {
          [e.target.id]: e.target.value,
          errors: {
            ...this.state.errors,
            [e.target.id]: ""
          }
        },
        () => {
          this.isDisable();
        }
      );
    }

    // this.setState(
    //   {
    //     [e.target.id]: e.target.value,
    //     errors: {
    //       ...this.state.errors,
    //       [e.target.id]: ""
    //     }
    //   },
    //   () => {
    //     this.isDisable();
    //   }
    // );

    // if (urlregex.test(e.target.value)) {
    // this.setState({ ['customize_url']: e.target.value.replace(/[^\w]/g, '') }, () => {
    //   this.isDisable();
    // });
    // }
    // return;
    // }

    // this.setState(
    //   {
    //     [e.target.id]: e.target.value,
    //     errors: {
    //       ...this.state.errors,
    //       [e.target.id]: ""
    //     }
    //   },
    //   () => {
    //     this.isDisable();
    //   }
    // );
  }

  isDisable = () => {
    let { customize_url, business_name, city } = this.state;
    if (!customize_url || !business_name || !city) {
      this.setState({ disable: true });
    } else {
      this.setState({ disable: false });
    }
  };
  render() {
    let { errors } = this.state;
    console.log("this.state: ", this.state);
    return (
      <div>
        <Header history={this.props.history} title="Your Business" />
        <ReactCSSTransitionGroup
          transitionName="example"
          transitionAppear={true}
          transitionAppearTimeout={1000}
          transitionEnter={false}
          transitionLeave={false}
        // transitionEnterTimeout={500}
        // transitionLeaveTimeout={300}
        >
          <section className="maincont aboutMaincont">
            <div className="container">
              <div className="col-lg-12 col-sm-12">
                <div className="oscar-logo-wrapper">
                  <img src={require("../../assets/shop.svg")} />
                </div>
              </div>

              <div className="">
                <div className="topSectionBox">
                  <div className="back-btn-wrapper">
                    <div
                      className="backarrow"
                      onClick={() => {
                        Firebase.customEvent("backToUserInfo");
                        this.props.changeView("CREATE_ACCOUNT");
                      }}
                    >
                      <img
                        className="backarrow-img"
                        src={require("../../assets/backarrow.svg")}
                      />
                    </div>
                  </div>

                  <div className="skipBoxMain">
                    {/* <SkipBtn history={this.props.history} /> */}
                  </div>
                  <div className="verification_topHeading">
                    <h1 className="formHeading">Tell us about your Business</h1>
                    <h1 className="blankSpace"></h1>
                  </div>
                </div>

                <form className="aboutaccountform" onSubmit={this.onSubmit}>
                  {/* <div className="drop_down_select_pos">
                    <SelectField
                      errorText={errors.business_type}
                      floatingLabelText="What type of business"
                      value={this.state.business_type}
                      onChange={(event, index, value) => {
                        this.setState({
                          errors: {
                            ...errors,
                            business_type: ""
                          },
                          business_type: value
                        });
                      }}
                      floatingLabelStyle={{ color: "#262626" }}
                      underlineFocusStyle={{ borderColor: "#9789dd" }}
                      underlineStyle={{ borderColor: "#E7E7EF" }}
                      selectedMenuItemStyle={{ color: "#9789dd" }}
                      labelStyle={{
                        color: "#262626",
                        paddingLeft: "12px",
                        paddingBottom: "10px"
                      }}
                    >
                      <MenuItem value="fashion" primaryText="Fashion" />
                      <MenuItem value="restaurant" primaryText="Restaurant" />
                      <MenuItem value="retail" primaryText="Retail Mart" />
                    </SelectField>
                  </div> */}

                  {/* <div className="drop_down_select_pos">
                    <SelectField
                      errorText={errors.selling_type}
                      floatingLabelText="Where are you selling today ?"
                      value={this.state.selling_type}
                      onChange={(event, index, value) => {
                        this.setState({
                          errors: {
                            ...errors,
                            selling_type: ""
                          },
                          selling_type: value
                        });
                      }}
                      floatingLabelStyle={{ color: "#262626" }}
                      underlineFocusStyle={{ borderColor: "#9789dd" }}
                      underlineStyle={{ borderColor: "#E7E7EF" }}
                      selectedMenuItemStyle={{ color: "#9789dd" }}
                      labelStyle={{
                        color: "#262626",
                        paddingLeft: "12px",
                        paddingBottom: "10px"
                      }}
                    >
                      <MenuItem value="ON_LINE" primaryText="Online" />
                      <MenuItem value="B_M" primaryText="Brick and Mortar" />
                    </SelectField>
                  </div> */}

                  <div className="inputStyleMainBx">
                    <TextField
                      maxlength="50"
                      minlength="3"
                      required
                      className="inputStyle"
                      id="business_name"
                      errorText={errors.business_name}
                      errorStyle={{
                        position: "relative",
                        bottom: "-1",
                        textAlign: "left"
                      }}
                      onChange={this.onChange}
                      value={this.state.business_name}
                      hintText=""
                      floatingLabelText="BUSINESS NAME"
                      floatingLabelStyle={{
                        color: "#b7b7b7",
                        fontSize: "14px",
                        height: "65px"
                      }}
                      underlineFocusStyle={{ borderColor: "#662d94" }}
                      underlineStyle={{ borderColor: "#E7E7EF" }}
                      inputStyle={{
                        color: "#510146",
                        paddingLeft: "12px",
                        paddingBottom: "5px",
                        paddingTop: "30px",
                        boxShadow: "none",
                        marginTop: "0px"
                      }}
                    />
                  </div>

                  <div className="inputStyleMainBx  passwordCont domainCont">
                    <FormControl style={{ marginTop: "20px" }}>
                      <InputLabel
                        floatingLabelStyle={{
                          color: "#af2d2d",
                          fontSize: "14px"
                        }}
                        floatingLabelStyle={{
                          color: "#262626",
                          fontSize: "30px"
                        }}
                        style={{ color: "#b7b7b7" }}
                      >
                        OSCAR URL
                      </InputLabel>
                      <Input
                        required
                        inputProps={{
                          minlength: 3,
                          maxlength: 50
                          // pattern:"^[A-Za-z]*$"
                        }}
                        className="inputStylePassword"
                        style={{ color: "#510146", textTransform: 'lowercase' }}
                        inputStyle={{
                          color: "#510146",
                          paddingLeft: "12px",
                          paddingBottom: "5px",
                          paddingTop: "30px",
                          boxShadow: "none",
                          marginTop: "0px"
                        }}
                        underlineFocusStyle={{ borderColor: "#662d94" }}
                        underlineStyle={{ borderColor: "#E7E7EF" }}
                        id="customize_url"
                        // disabled={true}
                        value={this.state.customize_url}
                        // value={this.state.business_name}
                        onChange={this.onChange}
                        error={errors.customize_url ? true : false}
                        endAdornment={
                          <InputAdornment position="end">
                            <div className="dotComname">.oscarpos.pk</div>
                          </InputAdornment>
                        }
                      />
                      {errors.customize_url ? (
                        <span
                          style={{
                            // position: "relative",
                            bottom: "15px",
                            fontSize: "12px",
                            // lineHeight: "12px",
                            color: "rgb(244, 67, 54)",
                            transition:
                              "all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms",
                            padding: "0px"
                          }}
                        >
                          Already exist try with another
                        </span>
                      ) : null}
                    </FormControl>
                  </div>

                  <SelectField
                    maxHeight={160}
                    selectedMenuItemStyle={{ color: "#662d94" }}
                    className="dropDownMain"
                    style={{
                      width: "100%",
                      textAlign: "left"
                    }}
                    floatingLabelStyle={{
                      color: "#262626"
                      // paddingLeft: "1em"
                      // paddingLeft: !this.state.city ? "15px" : "0"
                    }}
                    value={this.state.city}
                    onChange={(event, index, value) => {
                      this.setState(
                        {
                          errors: {
                            ...errors,
                            city: ""
                          },
                          city: value
                        },
                        () => {
                          this.isDisable();
                        }
                      );
                    }}
                    className="inputStyle dropDownInnermat"
                    floatingLabelText="City"
                    floatingLabelFixed={true}
                    floatingLabelStyle={{
                      color: "#b7b7b7",
                      fontSize: "14px",
                      height: "65px"
                    }}
                    underlineFocusStyle={{ borderColor: "#662d94" }}
                    underlineStyle={{ borderColor: "#E7E7EF" }}
                    inputStyle={{
                      color: "#510146",
                      paddingLeft: "9px",
                      paddingBottom: "4px",
                      paddingTop: "6px",
                      boxShadow: "none",
                      marginTop: "0px"
                    }}
                  >
                    {cities.map(data => {
                      return <MenuItem value={data} primaryText={data} />;
                    })}
                    {/* <MenuItem value="HYD" primaryText="Hyderabad" />
                  <MenuItem value="LHE" primaryText="Lahore" />
                  <MenuItem value="MUL" primaryText="Multan" /> */}
                  </SelectField>
                  {/* </div> */}

                  <div className="clear" />
                  <div className="text_box_fields">
                    <button
                      disabled={this.state.disable}
                      className="nextBtn"
                      type="submit"
                      style={{
                        background: this.state.disable ? "#d1d3d4" : "#4ac600"
                      }}
                    >
                      <div className="nextBtnText">NEXT</div>
                      <div className="nextBtnimg">
                        <img
                          className="nextBtn-img"
                          src={require("../../assets/nextarrow.svg")}
                        />
                      </div>
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </section>
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}

export default connect()(About);
