import React from "react";
import { connect } from "react-redux";
import "./style.css";
import TextField from "material-ui/TextField";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";
import Header from "../header";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import FormControl from "@material-ui/core/FormControl";
import Auth from "../../store/action/auth";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import Firebase from "../../Firebase/analyticsEvents";
// import SkipBtn from "../skipBtn";

class DineScreen extends React.Component {
  constructor(props) {
    super(props);
    let has_dine_in = JSON.parse(localStorage.getItem("dine")) || "";
    this.state = {
      selectedOption: has_dine_in
    };
  }
  componentDidMount() {
    Firebase.setScreen("onboarding:DineInScreen", "Dine");
  }

  next = () => {
    let { selectedOption } = this.state;
    if (selectedOption) {
      Firebase.customEvent("navigateToDeliveryTypeScreen");
      localStorage.setItem("dine", JSON.stringify(this.state.selectedOption));
      localStorage.setItem(
        "viewType",
        JSON.stringify({ viewType: "DELIVERY_TYPE" })
      );
      this.props.changeView("DELIVERY_TYPE");
    }
  };
  handleOptionChange = e => {
    console.log("e.target: ", e.target.value);
    Firebase.customEvent("dineInAvailable", {
      dineInAvailable: e.target.value
    });
    this.setState({ selectedOption: e.target.value });
  };
  render() {
    let { errors } = this.state;
    console.log("this.state: ", this.state);
    return (
      <div>
        <Header history={this.props.history} title="Your Business" />
        <ReactCSSTransitionGroup
          transitionName="example"
          transitionAppear={true}
          transitionAppearTimeout={1000}
          transitionEnter={false}
          transitionLeave={false}
          // transitionEnterTimeout={500}
          // transitionLeaveTimeout={300}
        >
          <section className="maincont aboutMaincont">
            <div className="container">
              <div className="col-lg-12 col-sm-12">
                <div className="oscar-logo-wrapper">
                  <img src={require("../../assets/dineIn-01.svg")} />
                </div>
              </div>

              <div className="">
                <div className="topSectionBox">
                  <div className="back-btn-wrapper">
                    <div
                      className="backarrow"
                      onClick={() => {
                        Firebase.customEvent("backToTabletScreen");
                        this.props.changeView("TABLET");
                      }}
                    >
                      <img
                        className="backarrow-img"
                        src={require("../../assets/backarrow.svg")}
                      />
                    </div>
                  </div>

                  {/* <div className="skipBoxMain">
                  <SkipBtn history={this.props.history} />
                </div> */}
                  <div className="verification_topHeading">
                    <h1 className="formHeading">Do you have Dine in?</h1>
                    <h1 className="blankSpace"></h1>
                  </div>
                </div>
              </div>

              <div>
                {/* <div className="businessHeading">BUSINESS CATEGORY</div> */}
                <div className="business-category-container tablet_screen">
                  <div className="radio">
                    <label>
                      <input
                        type="radio"
                        value="yes"
                        checked={this.state.selectedOption === "yes"}
                        onChange={this.handleOptionChange}
                      />
                      YES
                    </label>
                  </div>
                  <div className="radio">
                    <label>
                      <input
                        type="radio"
                        value="no"
                        checked={this.state.selectedOption === "no"}
                        onChange={this.handleOptionChange}
                      />
                      NO
                    </label>
                  </div>
                </div>
              </div>
              <div className="text_box_fields">
                <button
                  onClick={this.next}
                  disabled={!this.state.selectedOption}
                  className="nextBtn"
                  type="submit"
                  style={{
                    background: !this.state.selectedOption
                      ? "#d1d3d4"
                      : "#4ac600"
                  }}
                >
                  <div className="nextBtnText">NEXT</div>
                  <div className="nextBtnimg">
                    <img
                      className="nextBtn-img"
                      src={require("../../assets/nextarrow.svg")}
                    />
                  </div>
                </button>
              </div>
            </div>
          </section>
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}

export default connect()(DineScreen);
