import React from "react";
import "./styles.css";
import TextField from "material-ui/TextField";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";
import Header from "../header";

class ThankYou extends React.Component {
  render() {
    return (
      <React.Fragment>
        <section className="maincont">
          <div className="container">
            <div className="row">
              <div className="mainContainer">
                <div>

                <img src="4.png" />
                <p className="thankyou_text">Thank you!</p>
                <p className="paragraph-text">
                  Your request has been <br/> submitted <span>successfully</span>
                </p>
                </div>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}

export default ThankYou;
