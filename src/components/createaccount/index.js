import React from "react";
import axios from "axios";
import "./style.css";
import TextField from "material-ui/TextField";
import Header from "../header";
import { baseURL, patt, validateEmail } from "../../constants";
import Auth from "../../store/action/auth";
import { connect } from "react-redux";
import { Animated } from "react-animated-css";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import FormControl from "@material-ui/core/FormControl";
import ReCAPTCHA from "react-google-recaptcha";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import Firebase from "../../Firebase/analyticsEvents";

class CreateAccount extends React.Component {
  constructor(props) {
    super(props);
    var user_info = JSON.parse(localStorage.getItem("user_info")) || {};
    console.log("userInfo: ", user_info);
    this.state = {
      email: user_info.email || "",
      name: user_info.name || "",
      phone_number: user_info.phone_number || "",
      city: user_info.city || "",
      store_name: user_info.store_name || "",
      // password: user_info.password || "",
      password: "12345",
      errors: {},
      disable: Object.keys(user_info).length ? false : true
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.recaptchaRef = React.createRef();
    window.addEventListener("beforeunload", ev => {
      ev.preventDefault();
      return (ev.returnValue = "Are you sure you want to close?");
    });
  }
  componentDidMount() {
    Firebase.setScreen("onboarding:userinfo", "CreateAccount");
  }
  async onSubmit(e) {
    e.preventDefault();
    let {
      email,
      name,
      phone_number,
      city,
      store_name,
      password,
      isRecaptcha
    } = this.state;
    let recaptchaValue = this.recaptchaRef.current.getValue();
    let params = {
      email,
      name,
      username: email,
      phone_number,
      password,
      city
    };

    // if (!email || !name || !phone_number || !password || !recaptchaValue) {
    if (!email || !name || !phone_number || !recaptchaValue) {
      console.log("inside if");
      this.setState({
        errors: {
          email: !email ? "This field is required." : "",
          name: !name ? "This field is required." : "",
          phone_number: !phone_number ? "This field is required." : "",
          store_name: !store_name ? "This field is required." : "",
          city: !city ? "This field is required." : "",
          // password: !password ? "This field is required." : "",
          isRecaptcha: !isRecaptcha ? "Please confirm you are not a robot." : ""
        }
      });
      return;
    }
    if (!phone_number.match(patt)) {
      this.setState({
        errors: {
          phone_number: "Invalid Ph #. Retry with correct Ph # E.g 03001234567"
        },
        loading: false
      });
      return;
    }
    if (!validateEmail(email)) {
      this.setState({
        errors: {
          email: "Invalid Email"
        },
        loading: false
      });
      return;
    }
    localStorage.setItem("user_info", JSON.stringify(params));
    let param = {
      email,
      username: name,
      phone_number,
      password
      // check: true
    };

    try {
      let res = await this.props.dispatch(Auth.check({ phone_number }));
      if (!res.data.available) {
        this.setState({
          errors: {
            phone_number: "Phone # already exist please try with another"
          }
        });
        return;
      }
    } catch (err) {
      console.log("check phone_number catch: ", err);
    }

    this.props
      .dispatch(Auth.check({ email }))
      .then(res => {
        console.log("check user response: ", res);
        if (!res.data.available) {
          this.setState({
            errors: {
              email: "email already exist please try with another email"
            }
          });
          return;
        }
        localStorage.setItem(
          "viewType",
          JSON.stringify({ viewType: "VERIFICATION" })
        );
        Firebase.customEvent("navigateToCodeVerificationScreen", params);

        this.props.changeView("VERIFICATION");

        // this.props
        //   .dispatch(Auth.onCreateUser(param))
        //   .then(res => {
        //     console.log("res: ", res);
        //     this.props.changeView("ABOUT_YOURSELF");
        //   })
        //   .catch(err => {
        //     let errors = {};
        //     err = JSON.parse(JSON.stringify(err));
        //     if (err.response.status === 401) {
        //       localStorage.removeItem("token");
        //       alert("Session expired! Please try again");
        //       this.props.history.replace("/login");
        //       return;
        //     }
        //     if (err.response.data.email) {
        //       errors.email = err.response.data.email[0];
        //     }

        //     if (err.response.data.username) {
        //       errors.username = err.response.data.username[0];
        //     }
        //     if (err.response.data.store_name) {
        //       errors.store_name = err.response.data.store_name[0];
        //     }
        //     if (err.response.data.password) {
        //       errors.password = err.response.data.password[0];
        //     }

        //     this.setState({
        //       errors
        //     });

        //     console.log("err", JSON.parse(JSON.stringify(err)));
        //   });
      })
      .catch(err => {
        console.log("error: ", err);
      });
  }

  validateEmail(params) {
    axios.defaults.headers["Authorization"] = localStorage.getItem("token");
    return new Promise(function(resolve, reject) {
      axios
        .post(`${baseURL}accounts/create/`, params)
        .then(res => {
          if (res.status === 200) {
            resolve(res);
          } else {
            reject(res);
          }
        })
        .catch(err => {
          reject(err);
        });
    });
  }
  onChange(e) {
    // console.log("!isNaN(e.target.value): ", isNaN(e.target.value));
    let nameregex = RegExp("^[a-zA-Z'.-]+( [a-zA-Z'.-]+)* ?$");
    // if (e.target.id === "password") {
    //   this.setState({ [e.target.id]: e.target.value.trim() }, () => {
    //     let { email, name, phone_number, password, city } = this.state;
    //     if (!email || !name || !phone_number || !password) {
    //       this.setState({ disable: true });
    //       return;
    //     } else {
    //       this.setState({ disable: false });
    //     }
    //   });
    //   return;
    // }
    if (
      e.target.id === "phone_number" &&
      isNaN(e.target.value) &&
      e.target.value.length < 12
    ) {
      return;
    }

    if (
      e.target.id === "name" &&
      e.target.value.length &&
      !nameregex.test(e.target.value)
    ) {
      return;
    }
    this.setState(
      {
        [e.target.id]: e.target.value,
        errors: {
          ...this.state.errors,
          [e.target.id]: ""
        }
      },
      () => {
        let { email, name, phone_number, password, city } = this.state;
        // if (!email || !name || !phone_number || !password) {
        if (!email || !name || !phone_number) {
          this.setState({ disable: true });
          return;
        } else {
          this.setState({ disable: false });
        }
      }
    );
  }

  handleClickShowPassword = () => {
    this.setState({ showPassword: !this.state.showPassword });
  };

  handleMouseDownPassword = event => {
    event.preventDefault();
  };
  recaptchaonChange = e => {
    console.log("recaptcha onchange: ", e);
    const recaptchaValue = this.recaptchaRef.current.getValue();
    this.setState({ errors: { ...this.state.errors, isRecaptcha: "" } });
    if (recaptchaValue) {
      Firebase.customEvent("recaptchaVerified");
    }
    console.log("recaptcha value: ", recaptchaValue);
  };

  onExpired = data => {
    console.log("on captcha expire: ", data);
    this.recaptchaRef.current.reset();
    Firebase.customEvent("recaptchaExpired");
    console.log(
      "this.recaptchaRef.current.getValue(): ",
      this.recaptchaRef.current.getValue()
    );
  };
  render() {
    let { errors } = this.state;
    console.log("disable: ", this.state.disable);
    console.log("this.state.errors: ", this.state.errors);
    return (
      <div>
        <Header history={this.props.history} title="New Account" />
        <ReactCSSTransitionGroup
          transitionName="example"
          transitionAppear={true}
          transitionAppearTimeout={1000}
          transitionEnter={false}
          transitionLeave={false}
          // transitionEnterTimeout={500}
          // transitionLeaveTimeout={300}
        >
          <section className="maincont">
            <div className="container">
              <div className="">
                <div className="col-lg-12 col-sm-12 oscar-logo-wrapper new_Wrapper_box">
                  <img width='64' height='64' src={require("../../assets/nikki.png")} />
                </div>

                <div className="form-main">
                  <h1 className="formHeading">Hi, I am Nikki.</h1>

                  <p className="formHeading2">
                    I will help you set up your POS in 3 - 4 minutes. <br />
                    Ready to go?
                  </p>
                  <form className="crtaccountform" onSubmit={this.onSubmit}>
                    <div className="inputStyleMainBx">
                      <TextField
                        autoComplete="off"
                        required
                        maxlength="40"
                        minlength="3"
                        className="inputStyle"
                        errorText={errors.username}
                        value={this.state.name}
                        onChange={this.onChange}
                        id="name"
                        name="name"
                        hintText=""
                        floatingLabelText="FULL NAME"
                        type="text"
                        floatingLabelStyle={{
                          color: "#b7b7b7",
                          fontSize: "14px",
                          height: "60px"
                        }}
                        underlineFocusStyle={{ borderColor: "#662d94" }}
                        underlineStyle={{ borderColor: "#E7E7EF" }}
                        inputStyle={{
                          color: "#510146",
                          paddingLeft: "12px",
                          paddingBottom: "5px",
                          paddingTop: "30px",
                          boxShadow: "none",
                          marginTop: "0px"
                        }}
                      />
                    </div>
                    <div className="inputStyleMainBx">
                      <TextField
                        autoComplete="off"
                        hintText=""
                        className="inputStyle"
                        errorStyle={{
                          position: "relative",
                          bottom: "-1",
                          textAlign: "left"
                        }}
                        value={this.state.email}
                        id="email"
                        errorText={errors.email}
                        onChange={this.onChange}
                        required
                        floatingLabelText="Email"
                        type="email"
                        floatingLabelStyle={{
                          color: "#b7b7b7",
                          fontSize: "14px",
                          height: "65px"
                        }}
                        underlineFocusStyle={{ borderColor: "#662d94" }}
                        underlineStyle={{ borderColor: "#E7E7EF" }}
                        inputStyle={{
                          color: "#510146",
                          paddingLeft: "12px",
                          paddingBottom: "5px",
                          paddingTop: "30px",
                          boxShadow: "none",
                          marginTop: "0px"
                        }}
                      />
                    </div>
                    {/* <div className="field_bottom_para">
                        <p>
                          Note: Login credentials will share on the email. Please enter your valid email address. 
                        </p>
                    </div> */}
                    <div className="inputStyleMainBx">
                      <TextField
                        // type="number"
                        autoComplete="off"
                        maxlength="11"
                        required
                        className="inputStyle"
                        errorText={errors.phone_number}
                        errorStyle={{
                          position: "relative",
                          top: "10",
                          bottom: "-1",
                          textAlign: "left"
                        }}
                        value={this.state.phone_number}
                        name="phone_number"
                        onChange={this.onChange}
                        id="phone_number"
                        hintText=""
                        floatingLabelText="Phone Number"
                        floatingLabelStyle={{
                          color: "#b7b7b7",
                          fontSize: "14px",
                          height: "65px"
                        }}
                        underlineFocusStyle={{ borderColor: "#662d94" }}
                        underlineStyle={{ borderColor: "#E7E7EF" }}
                        inputStyle={{
                          color: "#510146",
                          paddingLeft: "12px",
                          paddingBottom: "5px",
                          paddingTop: "30px",
                          boxShadow: "none",
                          marginTop: "0px"
                        }}
                      />
                    </div>
                    {/* <div className="inputStyleMainBx passwordCont">
                      <FormControl style={{ marginTop: "24px" }}>
                        <InputLabel
                          floatingLabelStyle={{
                            color: "#af2d2d",
                            fontSize: "14px"
                          }}
                          floatingLabelStyle={{
                            color: "#262626",
                            fontSize: "30px"
                          }}
                          //  underlineFocusStyle={{ borderColor: "#662d94" }}
                          //  underlineStyle={{ borderColor: "#E7E7EF" }}
                          style={{ color: "#b7b7b7" }}
                          htmlFor="adornment-password"
                        >
                          Password
                        </InputLabel>
                        <Input
                          inputProps={{
                            minlength: 6,
                            maxlength: 20
                            // pattern:"^[A-Za-z]*$"
                          }}
                          required
                          className="inputStylePassword"
                          id="password"
                          style={{ color: "#510146" }}
                          inputStyle={{
                            color: "#510146",
                            paddingLeft: "12px",
                            paddingBottom: "5px",
                            paddingTop: "30px",
                            boxShadow: "none",
                            marginTop: "0px"
                          }}
                          // underlineFocusStyle={{ borderColor: "#662d94" }}
                          // underlineStyle={{ borderColor: "#E7E7EF" }}
                          error={errors.password ? true : false}
                          errorText={errors.password ? errors.password : null}
                          type={this.state.showPassword ? "text" : "password"}
                          value={this.state.password}
                          onChange={this.onChange}
                          endAdornment={
                            <InputAdornment
                              style={{
                                justifyContent: "flex-end"
                              }}
                              position="end"
                            >
                              <IconButton
                                aria-label="toggle password visibility"
                                onClick={this.handleClickShowPassword}
                                onMouseDown={this.handleMouseDownPassword}
                              >
                                {this.state.showPassword ? (
                                  <Visibility />
                                ) : (
                                  <VisibilityOff />
                                )}
                              </IconButton>
                            </InputAdornment>
                          }
                        />
                      </FormControl>
                    </div> */}
                    <div className="captchaBox_main">
                      <ReCAPTCHA
                        ref={this.recaptchaRef}
                        sitekey="6Ld4L7UcAAAAAFtW4FDvJyitg3tGzFJX2Gro4iOA"
                        onChange={this.recaptchaonChange}
                        onExpired={this.onExpired}
                      />
                      <div
                        style={{
                            color : "red",
                          visibility: this.state.errors.isRecaptcha
                            ? "visible"
                            : "hidden"
                        }}
                      >
                        {this.state.errors.isRecaptcha
                          ? this.state.errors.isRecaptcha
                          : ""}
                      </div>
                    </div>
                    <button
                      disabled={this.state.disable}
                      className="nextBtn"
                      type="submit"
                      style={{
                        background: this.state.disable ? "#d1d3d4" : "#4ac600"
                      }}
                    >
                      <div className="nextBtnText">NEXT</div>
                      <div className="nextBtnimg">
                        <img
                          className="nextBtn-img"
                          src={require("../../assets/nextarrow.svg")}
                        />
                      </div>
                    </button>
                  </form>

                  {/* <div className="bottom_text_para">
                    <p>
                      By entering your email, you agree to receive
                      communications from Oscar. For more details about how we
                      handle your personal information
                    </p>
                  </div> */}
                </div>
              </div>
            </div>
          </section>
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}

export default connect()(CreateAccount);
