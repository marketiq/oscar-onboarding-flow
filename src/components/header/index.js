import React from "react";
import "./styles.css";

class TopHeader extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      client : localStorage.getItem('client')
    }
  }
  routeToHome = () => {
    this.props.history.push("/");
  };
  render() {
    const { client } = this.state
    return (
      <div style={styles.topHeader}>
        <div>
          {client ==='restHero' ? 
          <img src={require("../../assets/rest_hero.png")} style={{marginTop: 5}}/>
          : <img src={require("../../assets/oscar-icon.svg")} />}
        </div>
        {/* <div className="header-title_text">
                {this.props.title}
            </div> */}
        {/* <div className="header-right_icon">
          <button onClick={this.routeToHome}>
            
          </button>
        </div> */}
      </div>
    );
  }
}

const styles = {
  logo: {
    // textAlign: 'center',
    // paddingTop: 12,
    // paddingLeft: 15,
    // width:'33%',
  },
  topHeader: {
    paddingLeft: 15,
    paddingRight: 15,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    // backgroundImage: 'url(/header_bg.jpg)',
    // backgroundColor: "#662d94",
    backgroundSize: "cover",
    //backgroundColor: '#652d92',
    color: "white",
    paddingTop: 0,
    height: "7vh",
    borderBottom: "2px solid #ededed"
  }
};

export default TopHeader;
