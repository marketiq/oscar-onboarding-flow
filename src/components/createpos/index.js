import React from "react";
import "./stylecrtpos.css";
import TextField from "material-ui/TextField";
import axios from "axios";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";
import Header from "../header";
import { baseURL } from "../../constants";
import Auth from "../../store/action/auth";
import { connect } from "react-redux";

class CreatePOS extends React.Component {
  constructor(props) {
    super(props);
    var license_info = JSON.parse(localStorage.getItem("license_info") || "{}");
    this.state = {
      errors: {},
      license_period: license_info.license_period || "",
      license_fee: license_info.license_fee || ""
    };
    this.onSubmit = this.onSubmit.bind(this);
  }
  onChange = e => {
    this.setState({
      errors: {
        ...this.state.errors,
        [e.target.id]: ""
      },
      [e.target.id]: e.target.value
    });
  };
  onSubmit(e) {
    e.preventDefault();
    let { license_fee, license_period } = this.state;

    if (!license_fee || !license_period) {
      this.setState({
        errors: {
          license_fee: !license_fee ? "This field is required." : "",
          license_period: !license_period ? "This field is required." : ""
        }
      });

      return;
    }
    let data = {
      license_fee: license_fee,
      license_period: license_period
    };
    localStorage.setItem("license_info", JSON.stringify(data));

    let params = {
      ...data,
      ...JSON.parse(localStorage.getItem("user_info")),
      ...JSON.parse(localStorage.getItem("user_about"))
    };
    this.props
      .dispatch(Auth.onCreateUser(params))
      .then(res => {
        localStorage.removeItem("license_info");
        localStorage.removeItem("user_info");
        localStorage.removeItem("user_about");
        this.props.changeView("THANK_YOU");
        setTimeout(() => {
          this.props.changeView("CREATE_ACCOUNT");
        }, 2000);
      })
      .catch(err => {
        if (err.response.status === 401) {
          localStorage.removeItem("token");
          alert("Session expired! Please try again");
          this.props.history.replace("/login");
          return;
        }
      });
  }
  onCreateUser(params) {
    axios.defaults.headers["Authorization"] = localStorage.getItem("token");
    return new Promise(function(resolve, reject) {
      axios
        .post(`${baseURL}accounts/create/`, params)
        .then(res => {
          if (res.status === 200) {
            resolve(res);
          } else {
            reject(res);
          }
        })
        .catch(err => {
          reject(err);
        });
    });
  }
  render() {
    let { errors } = this.state;
    return (
      <React.Fragment>
        <Header history={this.props.history} title="Licensing Fees" />

        <section className="maincont">
          <div className="container">
            <div className="row">
              <div className="col-lg-12 col-sm-12">
                <img src="3.png" />
              </div>
              <div className="col-lg-12">
                <h1 className="crtposheading">Choose Plan</h1>
              </div>

              <div className="col-lg-6 offset-lg-3">
                <form className="aboutaccountform" onSubmit={this.onSubmit}>
                  <div className="drop_down_select_pos">
                    <SelectField
                      required
                      errorText={errors.license_period}
                      errorStyle={{position : "relative", top : "10", bottom : "-1", textAlign : "left",}}
                      floatingLabelText="Select Licensing Period"
                      value={this.state.license_period}
                      onChange={(event, index, value) => {
                        this.setState({
                          errors: {
                            ...errors,
                            license_period: ""
                          },
                          license_period: value
                        });
                      }}
                      floatingLabelStyle={{ color: "#262626" }}
                      underlineFocusStyle={{ borderColor: "#9789dd" }}
                      underlineStyle={{ borderColor: "#E7E7EF" }}
                      selectedMenuItemStyle={{ color: "#9789dd" }}
                      labelStyle={{
                        color: "#262626",
                        paddingLeft: "12px",
                        paddingBottom: "10px"
                      }}
                    >
                      <MenuItem value="monthly" primaryText="Monthly" />
                      <MenuItem value="quarterly" primaryText="Quarterly" />
                      <MenuItem value="annually" primaryText="Annually" />
                    </SelectField>
                  </div>

                  <div className="drop_down_select_pos">
                    {/* <SelectField
                      value={this.state.currentPOS}
                      onChange={(event, index, value) => {
                        this.setState({ currentPOS: value });
                      }}
                      floatingLabelText="Current POS System?"
                      floatingLabelStyle={{ color: "#262626" }}
                      underlineFocusStyle={{ borderColor: "#9789dd" }}
                      underlineStyle={{ borderColor: "#E7E7EF" }}
                      selectedMenuItemStyle={{ color: "#9789dd" }}
                      labelStyle={{
                        color: "#262626",
                        paddingLeft: "12px",
                        paddingBottom: "10px"
                      }}
                    >
                      <MenuItem value={1} primaryText="Wordpress" />
                      <MenuItem value={2} primaryText="Magneto" />
                      <MenuItem value={3} primaryText="Etsy" />
                      <MenuItem value={4} primaryText="Wix" />
                      <MenuItem value={5} primaryText="BigCommerce" />
                      <MenuItem value={6} primaryText="WooCommerce" />
                      <MenuItem value={7} primaryText="Shopify" />
                      <MenuItem value={8} primaryText="Amazon" />
                    </SelectField> */}

                    <TextField
                      // required
                      errorText={errors.license_fee}
                      errorStyle={{position : "relative", top : "10", bottom : "-1", textAlign : "left",}}
                      id="license_fee"
                      onChange={this.onChange}
                      value={this.state.license_fee}
                      type="number"
                      hintText=""
                      floatingLabelText="Enter Licensing fee"
                      floatingLabelStyle={{ color: "#262626" }}
                      underlineFocusStyle={{ borderColor: "#9789dd" }}
                      underlineStyle={{ borderColor: "#E7E7EF" }}
                      inputStyle={{
                        color: "#262626",
                        paddingLeft: "12px",
                        paddingBottom: "10px",
                        boxShadow: "none"
                      }}
                    />
                  </div>
                  <button className="formBtn" type="submit">
                    CREATE MY POS
                  </button>
                </form>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}

export default connect()(CreatePOS);
