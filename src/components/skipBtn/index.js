import React from "react";
import "./index.css";

let SkipBtn = ({ history }) => {
  return <button className="skip-btn" onClick={() => history.replace("/")}>SKIP</button>;
};
export default SkipBtn;
