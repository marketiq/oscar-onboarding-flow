import React from "react";
import { connect } from "react-redux";
import "./index.css";
import TextField from "material-ui/TextField";
import Header from "../header";
import Auth from "../../store/action/auth";
import POS from "../../store/action/create_pos";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
// import SkipBtn from "../skipBtn";
import LoadingModal from "../LoadingModal";
import { patt, getImageSize } from "../../constants";

import Firebase from "../../Firebase/analyticsEvents";
const id = "file";
class About extends React.Component {
  constructor(props) {
    super(props);

    let user_about = JSON.parse(localStorage.getItem("user_about") || "{}");
    let user_info = JSON.parse(localStorage.getItem("user_info"));

    let business_category =
      JSON.parse(localStorage.getItem("business_category")) || "";
    this.state = {
      business_category,
      selectedBusiness: "",
      ntn_number: "",
      phone_number: (user_info && user_info.phone_number) || "",
      receipt_business_name: (user_about && user_about.business_name) || "",
      disable: ((user_info && !user_info.phone_number) || (user_about && !user_about.business_name)) ? true : false,
      errors: {},
      // file: null,
      file: '',
      loading: false,
      apiResponse: false,
      type: "loading"
    };
  }
  componentDidMount() {
    Firebase.setScreen("onboarding:CustomizeReceiptScreen", "CustomizeReceipt");
  }

  next = () => {
    console.time();

    // console.log("user_info: ", user_info);
    // console.log("user_about: ", user_about);
    // console.log("business_category: ", business_category);
    console.timeEnd();
  };

  toggleLoading = () => {
    this.setState({ loading: !this.state.loading });
  };
  onSubmit = async e => {
    e.preventDefault();
    // this.toggleLoading();
    let user_info = JSON.parse(localStorage.getItem("user_info"));
    let user_about = JSON.parse(localStorage.getItem("user_about"));
    let dine = JSON.parse(localStorage.getItem("dine"));
    let tablet = JSON.parse(localStorage.getItem("tablet"));
    let delivery_type = JSON.parse(localStorage.getItem("delivery_type"));
    let business_category = JSON.parse(
      localStorage.getItem("business_category")
    );
    let ntn_check_res = {};
    console.log("dine: ", dine);
    console.log("tablet: ", tablet);
    console.log("delivery_type", delivery_type);
    let { ntn_number, phone_number, receipt_business_name, file } = this.state;
    let params = {
      ...user_info,
      ...user_about,
      business_type: business_category,
      ntn_number,
      receipt_phone_number: phone_number,
      receipt_business_name,
      business_logo: file
    };

    console.log("user_info: ", user_info);
    console.log("user_about: ", user_about);
    console.log("business_category: ", business_category);
    // if (!receipt_business_name || !ntn_number || !phone_number || !file) {
    // if (!receipt_business_name || !phone_number || !file) {
    if (!receipt_business_name || !phone_number) {
      this.setState({
        errors: {
          phone_number: !phone_number ? "This field is required." : "",
          // ntn_number: !ntn_number ? "This field is required." : "",
          receipt_business_name: !receipt_business_name ? "This field is required." : ""
          // file: !file ? "This field is required." : ""
        }
      });
      return;
    }
    // if (!phone_number.match(patt)) {
    //   this.setState({
    //     errors: {
    //       phone_number: "Invalid Ph #. Retry with correct Ph # E.g 03001234567"
    //     },
    //     loading: false
    //   });
    //   return;
    // }
    if (ntn_number) {
      try {
        ntn_check_res = await this.props.dispatch(Auth.check({ ntn_number }));
      } catch (e) {
        console.log("catch of ntn check response: ", e);
      }
    }
    this.setState({ loading: true });
    console.log("check user response: ", ntn_check_res);
    if (ntn_number && !ntn_check_res.data.available) {
      this.setState({ errors: { ntn_number: "NTN already exist" } });
      // this.toggleLoading();
      return;
    }
    console.log("delivery_type: ", delivery_type);
    const formData = new FormData();
    formData.append("name", params.name);
    formData.append("email", params.email);
    formData.append("username", params.email);
    formData.append("password", params.password);
    formData.append("phone_number", user_info.phone_number);
    formData.append("business_name", params.business_name);
    formData.append("city", params.city);
    formData.append("customize_url", params.customize_url);
    formData.append("business_type", params.business_type);
    formData.append("business_logo", params.business_logo);
    formData.append("receipt_business_name", params.receipt_business_name);
    formData.append("receipt_phone_number", params.receipt_phone_number);
    formData.append("ntn_number", params.ntn_number);
    if (business_category === "Restaurant") {
      formData.append("has_dine_in", dine);
      formData.append("has_mps", tablet);
      formData.append("delivery_type", JSON.stringify(delivery_type));
    }
    // console.log("formdata: ", formData.getAll());
    this.props
      .dispatch(POS.createPOS(formData))
      .then(res => {
        Firebase.customEvent("createPOS", {
          ...params,
          phone_number: user_info.phone_number
        });
        console.log("pos created: ", res);
        localStorage.setItem("user_info", null);
        localStorage.setItem("user_about", null);
        localStorage.setItem("business_category", null);
        localStorage.setItem("viewType", null);
        if (business_category === "Restaurant") {
          localStorage.setItem("tablet", null);
          localStorage.setItem("dine", null);
          localStorage.setItem("delivery_type", null);
        }
        // this.toggleLoading();
        this.setState({ loading: true, type: "finished" }, () => {
          Firebase.setScreen("onboarding:ThankyouScreen", "Thankyou");
          Firebase.customEvent("POSsuccessfullyCreated", {
            ...params,
            phone_number: user_info.phone_number
          });
        });

        // this.props.history.replace("/");
      })
      .catch(err => {
        this.setState({ type: "loading", loading: false });
        console.log("error while creating pos: ", err);
      });
    // this.props.changeView("ABOUT_YOURSELF");
  };

  onChange = e => {
    let ntnregex = RegExp("^[a-zA-Z0-9]*$");
    if (e.target.id === "ntn_number" && !ntnregex.test(e.target.value)) {
      return;
    }
    if (
      e.target.id === "phone_number" &&
      isNaN(e.target.value) &&
      e.target.value.length < 12
    ) {
      return;
    }
    this.setState(
      {
        [e.target.id]: e.target.value,
        errors: {
          ...this.state.errors,
          [e.target.id]: ""
        }
      },
      () => {
        let { ntn_number, receipt_business_name, phone_number, file } = this.state;

        // if (!ntn_number || !receipt_business_name || !phone_number || !file) {
        // if (!receipt_business_name || !phone_number || !file) {
        if (!receipt_business_name || !phone_number) {
          this.setState({ disable: true });
          return;
        } else {
          this.setState({ disable: false });
        }
      }
    );
  };

  onChangeFile = e => {
    e.stopPropagation();
    e.preventDefault();

    let data = e.target.files[0];
    let allowTypes = ["png", "jpeg", "jpg"];
    let fileExtension = data.name.split(".");
    fileExtension = fileExtension[fileExtension.length - 1];
    if (allowTypes.indexOf(fileExtension) !== -1) {
      console.log("fileExtension: ", fileExtension);
      this.setState({ errors: { file: "" }, file: null });
      console.log("file: ", e.target.files);
      getImageSize(data).then(sizes => {
        console.log("resolved: ", sizes);
        // if (sizes.width <= 1024 && sizes.height <= 1024) {
        this.setState({ file: data }, () => {
          let {
            ntn_number,
            receipt_business_name,
            phone_number,
            file
          } = this.state;
          // if (!ntn_number || !receipt_business_name || !phone_number || !file) {
          // if (!receipt_business_name || !phone_number || !file) {
          if (!receipt_business_name || !phone_number) {
            this.setState({ disable: true });
            return;
          } else {
            this.setState({ disable: false });
          }
        });
        // } else {
        //   this.setState({
        //     errors: {
        //       file: "Please choose an image that's at least 1024x1024 pixels"
        //     }
        //   });
        // }
      });
    } else {
      this.setState({
        errors: {
          file: "please select png, jpg or jpeg file format"
        }
      });
    }
  };

  onDeleteImage = e => {
    e.stopPropagation();
    e.preventDefault();
    this.setState({ file: null });
  }

  closeModal = () => {
    this.setState({ loading: false }, () => {
      Firebase.customEvent("closeThankyouScreen", {
        closed: true
      });
      this.props.changeView("CREATE_ACCOUNT");
    });
  };
  render() {
    let { errors } = this.state;
    console.log("this.state: ", this.state);
    return (
      <div>
        {/* {this.state.loading ? <div className="laoding-view"></div> : null} */}
        <LoadingModal
          type={this.state.type}
          toggle={this.closeModal}
          isLoading={this.state.loading}
        />

        <Header history={this.props.history} title="Your Business" />
        <ReactCSSTransitionGroup
          transitionName="example"
          transitionAppear={true}
          transitionAppearTimeout={1000}
          transitionEnter={false}
          transitionLeave={false}
        // transitionEnterTimeout={500}
        // transitionLeaveTimeout={300}
        >
          <section className="maincont aboutMaincont">
            <div className="container">
              <div className="col-lg-12 col-sm-12">
                <div className="oscar-logo-wrapper">
                  <img src={require("../../assets/customize_receipt.svg")} />
                </div>
              </div>

              <div className="topSectionBox">
                <div className="back-btn-wrapper">
                  <div
                    className="backarrow"
                    onClick={() => {
                      // if (this.state.business_category === "Restaurant") {
                      //   Firebase.customEvent("backToDineInScreens");
                      //   this.props.changeView("DELIVERY_TYPE");
                      // } else {
                      //   Firebase.customEvent("BusinessCategoryScreen");
                      //   this.props.changeView("BUSINESS_CATEGORY");
                      // }
                      Firebase.customEvent("navigateToAboutYourBusinessScreen");
                      this.props.changeView("ABOUT_YOURSELF");
                    }}
                  >
                    <img
                      className="backarrow-img"
                      src={require("../../assets/backarrow.svg")}
                    />
                  </div>
                </div>

                {/* <div className="skipBoxMain">
                <SkipBtn history={this.props.history} />
              </div> */}
                <div className="verification_topHeading">
                  <h1 className="formHeading">
                    Provide business details to be printed on your receipt
                  </h1>
                  <h1 className="blankSpace"></h1>
                </div>
              </div>

              <form className="crtaccountform" onSubmit={this.onSubmit}>
                <div className="inputStyleMainBx">
                  <TextField
                    maxlength="40"
                    minlength="3"
                    className="inputStyle"
                    errorText={errors.receipt_business_name}
                    errorStyle={{
                      position: "relative",
                      top: "10",
                      bottom: "-1",
                      textAlign: "left"
                    }}
                    value={this.state.receipt_business_name}
                    onChange={this.onChange}
                    id="receipt_business_name"
                    name="receipt_business_name"
                    hintText=""
                    floatingLabelText="RECEIPT BUSINESS NAME"
                    type="text"
                    floatingLabelStyle={{
                      color: "#b7b7b7",
                      fontSize: "14px",
                      height: "60px"
                    }}
                    underlineFocusStyle={{ borderColor: "#662d94" }}
                    underlineStyle={{ borderColor: "#E7E7EF" }}
                    inputStyle={{
                      color: "#510146",
                      paddingLeft: "12px",
                      paddingBottom: "5px",
                      paddingTop: "30px",
                      boxShadow: "none",
                      marginTop: "0px"
                    }}
                  />
                </div>

                <div className="inputStyleMainBx">
                  <TextField
                    // type="number"
                    maxlength="11"
                    required
                    className="inputStyle"
                    errorText={errors.phone_number}
                    errorStyle={{
                      position: "relative",
                      top: "10",
                      bottom: "-1",
                      textAlign: "left"
                    }}
                    value={this.state.phone_number}
                    name="phone_number"
                    onChange={this.onChange}
                    id="phone_number"
                    hintText=""
                    floatingLabelText="Phone Number"
                    floatingLabelStyle={{
                      color: "#b7b7b7",
                      fontSize: "14px",
                      height: "60px"
                    }}
                    underlineFocusStyle={{ borderColor: "#662d94" }}
                    underlineStyle={{ borderColor: "#E7E7EF" }}
                    inputStyle={{
                      color: "#510146",
                      paddingLeft: "12px",
                      paddingBottom: "5px",
                      paddingTop: "30px",
                      boxShadow: "none",
                      marginTop: "0px"
                    }}
                  />
                </div>
                <div className="inputStyleMainBx">
                  <TextField
                    maxlength="20"
                    className="inputStyle"
                    hintText=""
                    errorStyle={{ padding: 0 }}
                    value={this.state.ntn_number}
                    id="ntn_number"
                    errorText={errors.ntn_number ? errors.ntn_number : null}
                    errorStyle={{
                      position: "relative",
                      top: "10",
                      bottom: "-1",
                      textAlign: "left"
                    }}
                    onChange={this.onChange}
                    // required
                    floatingLabelText="NTN NUMBER (Optional)"
                    type="ntn_number"
                    floatingLabelStyle={{
                      color: "#b7b7b7",
                      fontSize: "14px",
                      height: "60px"
                    }}
                    underlineFocusStyle={{ borderColor: "#662d94" }}
                    underlineStyle={{ borderColor: "#E7E7EF" }}
                    inputStyle={{
                      color: "#510146",
                      paddingLeft: "12px",
                      paddingBottom: "5px",
                      paddingTop: "30px",
                      boxShadow: "none",
                      marginTop: "0px"
                    }}
                  />
                </div>
                <div className="uploadButtonMain">
                  <input
                    id={id}
                    type="file"
                    accept="image/png, image/jpeg, image/jpg"
                    title=""
                    size="60"
                    onChange={this.onChangeFile}
                  />
                  <label
                    style={{
                      backgroundColor: this.state.file ? "#662d94" : "#fff"
                    }}
                    className="file-label"
                    htmlFor={id}
                  >
                    <div
                      style={{
                        flex: 8,
                        color: this.state.file ? "#fff" : "#262626"
                      }}
                    >
                      {this.state.file
                        ? "FILE UPLOADED"
                        : "UPLOAD BUSINESS LOGO (optional)"}
                    </div>
                    <div style={{ flex: 2 }} onClick={this.onDeleteImage}>
                      <img
                        // src={
                        //   this.state.file
                        //     ? require("../../assets/tick.svg")
                        //     : require("../../assets/upload.svg")
                        // }
                        src={
                          this.state.file
                            ? require("../../assets/close.svg")
                            : require("../../assets/upload.svg")
                        }
                        style={{ width: this.state.file ? '14px' : '24px' }}
                      />
                    </div>
                  </label>
                </div>
                <div
                  style={{
                    visibility: this.state.errors.file ? "visible" : "hidden",
                    color: "red"
                  }}
                >
                  <span className="fileExtension_error">
                    {this.state.errors.file}
                  </span>
                </div>
                <div className="uploadButtonFirstPara">
                  Upload a logo image in JPEG, PNG format, of size "1024x1024px"
                </div>
                {/* <div className="uploadButtonSecondPara">
                  Note: JPEG logo should be like '1024px X 1024px'
                </div> */}
                <button
                  disabled={this.state.disable}
                  className="nextBtn"
                  type="submit"
                  style={{
                    background: this.state.disable ? "#d1d3d4" : "#4ac600"
                  }}
                >
                  <div className="nextBtnText">NEXT</div>
                  <div className="nextBtnimg">
                    <img
                      className="nextBtn-img"
                      src={require("../../assets/nextarrow.svg")}
                    />
                  </div>
                </button>
              </form>
            </div>
          </section>
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}

export default connect()(About);
