import React from "react";
import { connect } from "react-redux";
import "./style.css";
import Header from "../header";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import Firebase from "../../Firebase/analyticsEvents";
// import SkipBtn from "../skipBtn";

class DeliveryScreen extends React.Component {
  constructor(props) {
    super(props);
    var delivery_type = JSON.parse(localStorage.getItem("delivery_type")) || {};
    console.log("delivery_type: ", delivery_type);
    let obj = {};
    for (let i = 0; i < delivery_type.length; i++) {
      obj[`${delivery_type[i]}`] = { selected: true };
    }
    this.state = {
      delivery_type: obj,
      disable: true
    };
  }
  componentDidMount() {
    Firebase.setScreen("onboarding:DeliveryScreen", "Delivery");
    let disable = true,
      { delivery_type } = this.state;
    for (let i in delivery_type) {
      if (Object.keys(delivery_type[i])) {
        disable = false;
      }
    }
    this.setState({ disable });
  }

  selectBusiness = type => {
    let { delivery_type } = this.state;
    console.log("delivery_type1; ", delivery_type);

    if (delivery_type[`${type}`] && delivery_type[`${type}`].selected) {
      delivery_type[`${type}`].selected = !delivery_type[`${type}`].selected;
    } else {
      delivery_type[`${type}`] = { selected: true };
    }
    console.log("delivery_type2; ", delivery_type);
    this.setState({ delivery_type }, () => {
      Firebase.customEvent("chooseDeliveryType", { delivery_type });
      let { delivery_type } = this.state;
      let disable = true;
      for (let i in delivery_type) {
        if (delivery_type[i] && delivery_type[i].selected) {
          disable = false;
          break;
        }
      }
      this.setState({ disable });
    });
  };

  getDeliveryTypes = () => {
    let keys = [],
      { delivery_type } = this.state;
    for (let i in delivery_type) {
      if (delivery_type[i].selected) {
        keys.push(i);
      }
    }
    return keys;
  };

  next = () => {
    let { delivery_type } = this.state;
    let keys = this.getDeliveryTypes();
    console.log("keys: ", keys);
    if (keys.length) {
      Firebase.customEvent("navigateToCustomizeReceipt", {
        selectedDelivery: keys
      });
      localStorage.setItem("delivery_type", JSON.stringify(keys));
      localStorage.setItem(
        "viewType",
        JSON.stringify({ viewType: "CUSTOMIZE_RECEIPT" })
      );
      this.props.changeView("CUSTOMIZE_RECEIPT");
    }
  };
  render() {
    let { errors } = this.state;
    console.log("this.state: ", this.state);
    return (
      <div>
        <Header history={this.props.history} title="Your Business" />
        <ReactCSSTransitionGroup
          transitionName="example"
          transitionAppear={true}
          transitionAppearTimeout={1000}
          transitionEnter={false}
          transitionLeave={false}
          // transitionEnterTimeout={500}
          // transitionLeaveTimeout={300}
        >
          <section className="maincont aboutMaincont">
            <div className="container">
              <div className="col-lg-12 col-sm-12">
                <div className="oscar-logo-wrapper">
                  <img src={require("../../assets/delivery.svg")} />
                </div>
              </div>
              <div className="">
                <div className="topSectionBox">
                  <div className="back-btn-wrapper">
                    <div
                      className="backarrow"
                      onClick={() => {
                        Firebase.customEvent("backToDineInScreen");
                        this.props.changeView("DINE");
                      }}
                    >
                      <img
                        className="backarrow-img"
                        src={require("../../assets/backarrow.svg")}
                      />
                    </div>
                  </div>

                  {/* <div className="skipBoxMain">
                  <SkipBtn history={this.props.history} />
                </div> */}
                  <div className="verification_topHeading">
                    <h1 className="formHeading">How do you deliver?</h1>
                    <h1 className="blankSpace"></h1>
                  </div>
                </div>
              </div>

              <div>
                {/* <div className="businessHeading">BUSINESS CATEGORY</div> */}
                <div className="business-category-container foodpanda_category_container">
                   <div
                    onClick={() => this.selectBusiness("foodpanda")}
                    className="business-category-item"
                    style={{
                      boxShadow:
                        this.state.delivery_type["foodpanda"] &&
                        this.state.delivery_type["foodpanda"].selected
                          ? "0 1px 1px rgba(231, 191, 226, 0.9)"
                          : "",
                      border:
                        this.state.delivery_type["foodpanda"] &&
                        this.state.delivery_type["foodpanda"].selected
                          ? "1px solid #e7bfe2"
                          : "",
                      background:
                        this.state.delivery_type["foodpanda"] &&
                        this.state.delivery_type["foodpanda"].selected
                          ? "#f8f8f8"
                          : ""
                    }}
                  >
                    <div className="business-category-name-img food_panda_img">
                      <img src={require("../../assets/foodpanda_Logo.png")} />
                    </div>
                     <div className="business-category-name">Food Panda</div> 
                  </div> 
                  {/* <div
                    onClick={() => this.selectBusiness("talabat")}
                    className="business-category-item"
                    style={{
                      boxShadow:
                        this.state.delivery_type["talabat"] &&
                        this.state.delivery_type["talabat"].selected
                          ? "0 1px 1px rgba(231, 191, 226, 0.9)"
                          : "",
                      border:
                        this.state.delivery_type["talabat"] &&
                        this.state.delivery_type["talabat"].selected
                          ? "1px solid #e7bfe2"
                          : "",
                      background:
                        this.state.delivery_type["talabat"] &&
                        this.state.delivery_type["talabat"].selected
                          ? "#f8f8f8"
                          : ""
                    }}
                  >
                    <div className="business-category-name-img food_panda_img">
                      <img src={require("../../assets/talabat.png")} />
                    </div>
                    <div className="business-category-name">Food Panda</div>
                  </div> */}
                  {/* <div
                    onClick={() => this.selectBusiness("eat_mubarak")}
                    className="business-category-item"
                    style={{
                      boxShadow:
                        this.state.delivery_type["eat_mubarak"] &&
                        this.state.delivery_type["eat_mubarak"].selected
                          ? "0 1px 1px rgba(231, 191, 226, 0.9)"
                          : "",
                      border:
                        this.state.delivery_type["eat_mubarak"] &&
                        this.state.delivery_type["eat_mubarak"].selected
                          ? "1px solid #e7bfe2"
                          : "",
                      background:
                        this.state.delivery_type["eat_mubarak"] &&
                        this.state.delivery_type["eat_mubarak"].selected
                          ? "#f8f8f8"
                          : ""
                    }}
                  >
                    <div className="business-category-name-img food_panda_img">
                      <img src={require("../../assets/eatMubarak_Logo.png")} />
                    </div>
                    {/* <div className="business-category-name">Eat Mubarak</div> *
                  </div> */}
                  {/* <div
                    onClick={() => this.selectBusiness("deliveroo")}
                    className="business-category-item"
                    style={{
                      boxShadow:
                        this.state.delivery_type["deliveroo"] &&
                        this.state.delivery_type["deliveroo"].selected
                          ? "0 1px 1px rgba(231, 191, 226, 0.9)"
                          : "",
                      border:
                        this.state.delivery_type["deliveroo"] &&
                        this.state.delivery_type["deliveroo"].selected
                          ? "1px solid #e7bfe2"
                          : "",
                      background:
                        this.state.delivery_type["deliveroo"] &&
                        this.state.delivery_type["deliveroo"].selected
                          ? "#f8f8f8"
                          : ""
                    }}
                  >
                    <div className="business-category-name-img food_panda_img">
                      <img src={require("../../assets/deliveroo.svg")} />
                    </div> */}
                    {/* <div className="business-category-name">Eat Mubarak</div> */}
                  {/* </div> */}
                  <div
                    onClick={() => this.selectBusiness("no_delivery")}
                    className="business-category-item"
                    style={{
                      boxShadow:
                        this.state.delivery_type["no_delivery"] &&
                        this.state.delivery_type["no_delivery"].selected
                          ? "0 1px 1px rgba(231, 191, 226, 0.9)"
                          : "",
                      border:
                        this.state.delivery_type["no_delivery"] &&
                        this.state.delivery_type["no_delivery"].selected
                          ? "1px solid #e7bfe2"
                          : "",
                      background:
                        this.state.delivery_type["no_delivery"] &&
                        this.state.delivery_type["no_delivery"].selected
                          ? "#f8f8f8"
                          : ""
                    }}
                  >
                    <div className="business-category-name-img">
                      <img src={require("../../assets/rider-01.svg")} />
                    </div>
                    <div className="business-category-name">Your Rider</div>
                  </div>
                  <div
                    // onClick={() => this.selectBusiness("no_delivery")}
                    // className="business-category-item"
                    // style={{
                    //   boxShadow:
                    //     this.state.delivery_type["no_delivery"] &&
                    //     this.state.delivery_type["no_delivery"].selected
                    //       ? "0 1px 1px rgba(231, 191, 226, 0.9)"
                    //       : "",
                    //   border:
                    //     this.state.delivery_type["no_delivery"] &&
                    //     this.state.delivery_type["no_delivery"].selected
                    //       ? "1px solid #e7bfe2"
                    //       : "",
                    //   background:
                    //     this.state.delivery_type["no_delivery"] &&
                    //     this.state.delivery_type["no_delivery"].selected
                    //       ? "#f8f8f8"
                    //       : ""
                    // }}
                  >
                    {/* <div className="business-category-name-img">
                      <img src={require("../../assets/no-Riderr.svg")} />
                    </div> */}
                    {/* <div className="business-category-name">No Delivery</div> */}
                  </div>
                </div>
              </div>
              <div className="text_box_fields">
                <button
                  onClick={this.next}
                  disabled={this.state.disable}
                  className="nextBtn"
                  type="submit"
                  style={{
                    background: this.state.disable ? "#d1d3d4" : "#4ac600"
                  }}
                >
                  <div className="nextBtnText">NEXT</div>
                  <div className="nextBtnimg">
                    <img
                      className="nextBtn-img"
                      src={require("../../assets/nextarrow.svg")}
                    />
                  </div>
                </button>
              </div>
            </div>
          </section>
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}

export default connect()(DeliveryScreen);
