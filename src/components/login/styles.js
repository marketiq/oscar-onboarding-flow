import  {
    floatingStyles,
    focusStyles,
    inputStyles,
    labelStyles
  } from 'floating-label-react';

  const styles = {
    floating: {
      ...floatingStyles,
      color: '#fff',
      fontSize : 14,
      paddingLeft : 15,
      paddingRight : 15,
      
    },
    focus: {
      ...focusStyles,
      borderColor: '#fff',
    },
    input: {
      ...inputStyles,
      borderBottomWidth: 1,
      borderBottomColor: '#fff',
      width: '100%',
      backgroundColor : 'transparent',
      color : '#fff',
      paddingLeft : 15,
      paddingRight : 15,
      paddingTop: 20,
    },
    placeholder : {
      backgroundColor : '#000',
      color : '#000',
    },
    label: {
      ...labelStyles,
      marginTop: '.5em',
      width: '100%',
      color : '#fff',
     
    }
  }

  export default styles;