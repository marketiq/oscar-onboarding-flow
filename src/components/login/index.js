import React from "react";
// import inputStyle from "./styles";
import "./styles.css";
import TextField from "material-ui/TextField";
// import { connect } from "react-redux";
// import { onLogin } from "../../actions";
import axios from "axios";
import Auth from "../../store/action/auth";
import { baseURL } from "../../constants";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      Loading: false,
      error: false
    };
  }
  onChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };
  onSubmit = e => {
    let { email, password } = this.state;
    this.setState({
      loading: true,
      error: false
    });
    e.preventDefault();
    let params = {
      username: email,
      email,
      password
    };
    axios
      .post(`${baseURL}auth-jwt/`, params)
      .then(res => {
        localStorage.setItem("token", "JWT " + res.data.token);
        axios.defaults.headers["Authorization"] = "JWT " + res.data.token;
        this.props.history.replace("/");
      })
      .catch(error => {
        this.setState({ loading: false, error: true });
      });
  };

  render() {
    return (
      <div className="loginMainCont">
        <div className="loginInnerCont">
          <p>
            <img
              src="/oscar.png"
              className="oscar-logo"
              alt="Logo"
              title="Logo"
            />
          </p>
          <form onSubmit={this.onSubmit}>
            <div>
              <div className="text_box_field">
                <TextField
                  required
                  id="email"
                  // type="email"
                  onChange={this.onChange}
                  value={this.state.email}
                  hintText=""
                  floatingLabelText="Email"
                  floatingLabelStyle={{ color: "#ffff" }}
                  underlineFocusStyle={{ borderColor: "#9789dd" }}
                  underlineStyle={{ borderColor: "#E7E7EF" }}
                  inputStyle={{
                    color: "#ffff",
                    paddingLeft: "12px",
                    paddingBottom: "10px",
                    boxShadow: "none"
                  }}
                />
              </div>
              <div className="text_box_field">
                <TextField
                  required
                  id="password"
                  type="password"
                  onChange={this.onChange}
                  value={this.state.password}
                  hintText=""
                  floatingLabelText="Password"
                  floatingLabelStyle={{ color: "#ffff" }}
                  underlineFocusStyle={{ borderColor: "#9789dd" }}
                  underlineStyle={{ borderColor: "#E7E7EF" }}
                  inputStyle={{
                    color: "#ffff",
                    paddingLeft: "12px",
                    paddingBottom: "10px",
                    boxShadow: "none"
                  }}
                />
              </div>
            </div>
            <div className="errorText">
              {this.state.error
                ? "The email or password you entered is incorrect. Please try again"
                : null}
            </div>
            <div className="loginBtnCont">
              <button type="submit" className="loginButton hvr-sweep-to-right">
                {this.state.loading ? (
                  <React.Fragment>
                    <i className="fa fa-circle-o-notch fa-spin" />
                  </React.Fragment>
                ) : (
                  "Login"
                )}
              </button>
            </div>
           
          </form>
        </div>
      </div>
    );
  }
}

export default Login;
